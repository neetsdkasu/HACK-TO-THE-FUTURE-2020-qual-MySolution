// Try HACK TO THE FUTURE 2020予選
// author: Leonardone @ NEETSDKASU

fn main() {
  let mut stdin = String::new();
  std::io::Read::read_to_string(&mut std::io::stdin(), &mut stdin).unwrap();
  let mut stdin = stdin.split_whitespace();
  let mut get = || stdin.next().unwrap();
  macro_rules! get {
    ($t:ty) => (get().parse::<$t>().unwrap());
    () => (get!(usize));
  }
  let n = get!();
  let m = get!();
  let b = get!();
  let gy = get!();
  let gx = get!();
  let mut robots = vec![];
  for _ in 0 .. m {
      let ry = get!();
      let rx = get!();
      let c = get().as_bytes()[0];
      robots.push((rx, ry, c));
  }
  let mut field = vec![vec![0; n]; n];
  for _ in 0 .. b {
      let by = get!();
      let bx = get!();
      field[by][bx] = -1;
  }
  let mut moves = vec![];
  
  for y in 0 .. n {
      for x in 0 .. n {
          if x != gx && y != gy {
              for &(rx, ry, _) in robots.iter() {
                  if rx == x && ry == y {
                      if rx < gx {
                          moves.push((x, y, b'R'));
                      } else if rx > gx {
                          moves.push((x, y, b'L'));
                      }
                      field[y][x] = 1;
                      break;
                  }
              }
              continue;
          }
          if field[y][x] < 0 {
              continue;
          }
          field[y][x] = 1;
          if x == gx {
              if y > gy {
                  moves.push((x, y, b'U'));
              } else if y < gy {
                  moves.push((x, y, b'D'));
              }
          }
          if y == gy {
              if x > gx {
                  moves.push((x, y, b'L'));
              } else if x < gx {
                  moves.push((x, y, b'R'));
              }
          }
      }
  }

  for y in 0 .. n {
      for x in 0 .. n {
          if field[y][x] >= 0 {
              continue;
          }
          if x > 0 && field[y][x-1] == 0 {
              if y < gy {
                  moves.push((x-1, y, b'D'));
              } else if y > gy {
                  moves.push((x-1, y, b'U'));
              }
              field[y][x-1] = 1;
          }
          if x < n-1 && field[y][x+1] == 0 {
              if y < gy {
                  moves.push((x+1, y, b'D'));
              } else if y > gy {
                  moves.push((x+1, y, b'U'));
              }
              field[y][x+1] = 1;
          }
          if y > 0 && field[y-1][x] == 0 {
              if x < gx {
                  moves.push((x, y-1, b'R'));
              } else if x > gx {
                  moves.push((x, y-1, b'L'));
              }
              field[y-1][x] = 1;
          }
          if y < n-1 && field[y+1][x] == 0 {
              if x < gx {
                  moves.push((x, y+1, b'R'));
              } else if x > gx {
                  moves.push((x, y+1, b'L'));
              }
              field[y+1][x] = 1;
          }
      }
  }
    
  println!("{}", moves.len());
  for &(x, y, c) in moves.iter() {
      println!("{} {} {}", y, x, c as char);
  }
}
